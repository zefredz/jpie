package com.sootNsmoke.prolog;

import java.io.*;

/**
 * @author Joshua Engel
 * @author Fr�d�ric Minne
 */
public class PrologLexer extends StreamTokenizer
{
	/**
	 * PrologLexer constructor using an InputStream
	 * @param is InputStream of the Prolog program
	 */
	public PrologLexer(InputStream is)
	{
		// super(is);						// Deprecated !!!!
		super(new BufferedReader(new InputStreamReader(is)));
		initLexer();
	}

	/**
	 * PrologLexer constructor using a String
	 * @param theory the Prolog program
	 */
	public PrologLexer(String theory)
	{
		super(new BufferedReader(new StringReader(theory)));
		initLexer();
	}

	/**
	 * Initialize the PrologLexer
	 */
	private void initLexer()
	{
		wordChars(':', ':');
		wordChars('-', '-');
		wordChars('_', '_');
		ordinaryChar('.');
		ordinaryChars('0', '9');
		wordChars('0', '9');
		slashSlashComments(true);
		slashStarComments(true);
	}

	/**
	 * Returns the next word in the program
	 * @return the next word in the program
	 * @throws IOException
	 */
	public String nextWord() throws IOException
	{
		int x = nextToken();
		if (x == TT_EOF)
			throw new IOException("EOF");
		else
			if (x == TT_WORD || x == '"')
				return sval;
			else
			{
				return new Character((char) x).toString();
			}
	}
}
