package com.sootNsmoke.prolog;

public class SyntaxError extends RuntimeException
{
	public SyntaxError(PrologLexer lex, String msg)
	{
		super(lex.lineno() + ": " + msg);
	}
}