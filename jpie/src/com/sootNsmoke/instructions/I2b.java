package com.sootNsmoke.instructions;

public class I2b extends NoArgsSequence
{
    public I2b()
    {
        super(0, 0, opc_i2b);
    }
}
