package com.sootNsmoke.instructions;

public class Ladd extends NoArgsSequence
{
    public Ladd()
    {
        super(0, -2, opc_ladd);
    }
}
