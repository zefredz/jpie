package com.sootNsmoke.instructions;

public class Lcmp extends NoArgsSequence
{
    public Lcmp()
    {
        super(0, -3, opc_lcmp);
    }
}
