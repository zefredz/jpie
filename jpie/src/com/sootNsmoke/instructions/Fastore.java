package com.sootNsmoke.instructions;

public class Fastore extends NoArgsSequence
{
    public Fastore()
    {
        super(0, -3, opc_fastore);
    }
}
