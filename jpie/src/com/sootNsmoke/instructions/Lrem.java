package com.sootNsmoke.instructions;

public class Lrem extends NoArgsSequence
{
    public Lrem()
    {
        super(0, -2, opc_lrem);
    }
}
