package com.sootNsmoke.instructions;

public class Fcmpg extends NoArgsSequence
{
    public Fcmpg()
    {
        super(0, -1, opc_fcmpg);
    }
}
