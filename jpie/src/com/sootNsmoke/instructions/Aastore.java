package com.sootNsmoke.instructions;

public class Aastore extends NoArgsSequence
{
    public Aastore()
    {
        super(0, -3, opc_aastore);
    }
}
