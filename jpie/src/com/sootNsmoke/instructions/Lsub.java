package com.sootNsmoke.instructions;

public class Lsub extends NoArgsSequence
{
    public Lsub()
    {
        super(0, -2, opc_lsub);
    }
}
