package com.sootNsmoke.instructions;

public class Dadd extends NoArgsSequence
{
    public Dadd()
    {
        super(0, -2, opc_dadd);
    }
}
