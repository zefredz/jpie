package com.sootNsmoke.instructions;

public class Dneg extends NoArgsSequence
{
    public Dneg()
    {
        super(0, 0, opc_dneg);
    }
}
