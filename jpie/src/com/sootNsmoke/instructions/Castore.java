package com.sootNsmoke.instructions;

public class Castore extends NoArgsSequence
{
    public Castore()
    {
        super(0, -3, opc_castore);
    }
}
