package com.sootNsmoke.instructions;

public class Drem extends NoArgsSequence
{
    public Drem()
    {
        super(0, -2, opc_drem);
    }
}
