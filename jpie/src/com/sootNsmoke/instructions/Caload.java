package com.sootNsmoke.instructions;

public class Caload extends NoArgsSequence
{
    public Caload()
    {
        super(0, -1, opc_caload);
    }
}
