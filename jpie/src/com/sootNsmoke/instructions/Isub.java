package com.sootNsmoke.instructions;

public class Isub extends NoArgsSequence
{
    public Isub()
    {
        super(0, -1, opc_isub);
    }
}
