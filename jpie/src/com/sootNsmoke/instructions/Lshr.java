package com.sootNsmoke.instructions;

public class Lshr extends NoArgsSequence
{
    public Lshr()
    {
        super(0, -1, opc_lshr);
    }
}
