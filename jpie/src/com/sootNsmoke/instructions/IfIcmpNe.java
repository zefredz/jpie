package com.sootNsmoke.instructions;

public class IfIcmpNe  extends  LabelSequence
{
    public IfIcmpNe (String label)
    {
        super(0, -2, opc_if_icmpne, label);
    }
}
