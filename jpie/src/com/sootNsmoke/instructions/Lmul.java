package com.sootNsmoke.instructions;

public class Lmul extends NoArgsSequence
{
    public Lmul()
    {
        super(0, -2, opc_lmul);
    }
}
