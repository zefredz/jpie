package com.sootNsmoke.instructions;

public class Swap  extends  NoArgsSequence
{
    public Swap ()
    {
        super(0, 0, opc_swap);
    }
}
