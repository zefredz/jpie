package com.sootNsmoke.instructions;

public class Idiv extends NoArgsSequence
{
    public Idiv()
    {
        super(0, -1, opc_idiv);
    }
}
