package com.sootNsmoke.instructions;

public class Fcmpl extends NoArgsSequence
{
    public Fcmpl()
    {
        super(0, -1, opc_fcmpl);
    }
}
