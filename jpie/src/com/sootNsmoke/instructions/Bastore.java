package com.sootNsmoke.instructions;

public class Bastore extends NoArgsSequence
{
    public Bastore()
    {
        super(0, -3, opc_bastore);
    }
}
