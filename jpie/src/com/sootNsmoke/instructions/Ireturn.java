package com.sootNsmoke.instructions;

public class Ireturn extends NoArgsSequence
{
    public Ireturn()
    {
        super(0, -1, opc_ireturn);
    }
}
