package com.sootNsmoke.instructions;

public class Ifeq  extends  LabelSequence
{
    public Ifeq (String label)
    {
        super(0, -1, opc_ifeq, label);
    }
}
