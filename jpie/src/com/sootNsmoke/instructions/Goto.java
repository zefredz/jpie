package com.sootNsmoke.instructions;

public class Goto extends LabelSequence
{
    public Goto(String cont)
    {
        super(0, 0, opc_goto, cont);
    }
}

