package com.sootNsmoke.instructions;

public class L2i extends NoArgsSequence
{
    public L2i()
    {
        super(0, -1, opc_l2i);
    }
}
