package com.sootNsmoke.instructions;

public class Ishl extends NoArgsSequence
{
    public Ishl()
    {
        super(0, -1, opc_ishl);
    }
}
