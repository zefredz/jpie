package com.sootNsmoke.instructions;

public class Fdiv extends NoArgsSequence
{
    public Fdiv()
    {
        super(0, -1, opc_fdiv);
    }
}
