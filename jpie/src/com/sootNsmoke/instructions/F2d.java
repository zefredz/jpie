package com.sootNsmoke.instructions;

public class F2d extends NoArgsSequence
{
    public F2d()
    {
        super(0, 1, opc_f2d);
    }
}
