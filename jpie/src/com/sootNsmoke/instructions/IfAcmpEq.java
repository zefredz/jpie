package com.sootNsmoke.instructions;

public class IfAcmpEq  extends LabelSequence
{
    public IfAcmpEq (String label)
    {
        super(0, -1, opc_if_acmpeq, label);
    }
}
