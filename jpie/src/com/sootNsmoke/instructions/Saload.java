package com.sootNsmoke.instructions;

public class Saload  extends  NoArgsSequence
{
    public Saload ()
    {
        super(0, -1, opc_saload);
    }
}
