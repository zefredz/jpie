package com.sootNsmoke.instructions;

public class ArrayLength  extends  NoArgsSequence
{
    public ArrayLength ()
    {
        super(0, 0, opc_arraylength);
    }
}
