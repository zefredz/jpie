package com.sootNsmoke.instructions;

public class Lreturn extends NoArgsSequence
{
    public Lreturn()
    {
        super(0, -2, opc_lreturn);
    }
}
