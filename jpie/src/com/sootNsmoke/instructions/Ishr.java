package com.sootNsmoke.instructions;

public class Ishr extends NoArgsSequence
{
    public Ishr()
    {
        super(0, -1, opc_ishr);
    }
}
