package com.sootNsmoke.instructions;

public class Dreturn extends NoArgsSequence
{
    public Dreturn()
    {
        super(0, -2, opc_dreturn);
    }
}
