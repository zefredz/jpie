package com.sootNsmoke.instructions;

public class Ixor extends NoArgsSequence
{
    public Ixor()
    {
        super(0, -1, opc_ixor);
    }
}
