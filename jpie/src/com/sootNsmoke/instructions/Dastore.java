package com.sootNsmoke.instructions;

public class Dastore extends NoArgsSequence
{
    public Dastore()
    {
        super(0, -4, opc_dastore);
    }
}
