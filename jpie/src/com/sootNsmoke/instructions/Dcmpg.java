package com.sootNsmoke.instructions;

public class Dcmpg extends NoArgsSequence
{
    public Dcmpg()
    {
        super(0, -3, opc_dcmpg);
    }
}
