package com.sootNsmoke.instructions;

public class Dup_x1 extends  NoArgsSequence
{
    public Dup_x1()
    {
        super(1, 1, opc_dup_x1);
    }
}
