package com.sootNsmoke.instructions;

public class I2l extends NoArgsSequence
{
    public I2l()
    {
        super(0, 1, opc_i2l);
    }
}
