package com.sootNsmoke.instructions;

public class Iushr extends NoArgsSequence
{
    public Iushr()
    {
        super(0, -1, opc_iushr);
    }
}
