package com.sootNsmoke.instructions;

public class Baload extends NoArgsSequence
{
    public Baload()
    {
        super(0, -1, opc_baload);
    }
}
