package com.sootNsmoke.instructions;

public class Lastore extends NoArgsSequence
{
    public Lastore()
    {
        super(0, -4, opc_lastore);
    }
}
