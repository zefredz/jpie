package com.sootNsmoke.instructions;

public class Lxor extends NoArgsSequence
{
    public Lxor()
    {
        super(0, -2, opc_lxor);
    }
}
