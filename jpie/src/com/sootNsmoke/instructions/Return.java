package com.sootNsmoke.instructions;

public class Return  extends  NoArgsSequence
{
    public Return ()
    {
        super(0, 0, opc_return);
    }
}
