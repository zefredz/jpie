package com.sootNsmoke.instructions;

public class Getstatic extends FieldSequence
{
    public Getstatic(String classname, String field_name,
                         String signature)
    {
        super(1, 1, opc_getstatic, classname, field_name, signature);
    }
}
