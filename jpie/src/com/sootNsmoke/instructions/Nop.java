package com.sootNsmoke.instructions;

public class Nop  extends  NoArgsSequence
{
    public Nop ()
    {
        super(0, 0, opc_nop);
    }
}
