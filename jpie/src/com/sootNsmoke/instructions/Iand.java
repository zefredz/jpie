package com.sootNsmoke.instructions;

public class Iand extends NoArgsSequence
{
    public Iand()
    {
        super(0, -1, opc_iand);
    }
}
