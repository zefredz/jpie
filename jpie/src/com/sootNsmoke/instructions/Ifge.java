package com.sootNsmoke.instructions;

public class Ifge  extends  LabelSequence
{
    public Ifge (String label)
    {
        super(0, -1, opc_ifge, label);
    }
}
