package com.sootNsmoke.instructions;

public class D2l extends NoArgsSequence
{
    public D2l()
    {
        super(0, 0, opc_d2l);
    }
}
