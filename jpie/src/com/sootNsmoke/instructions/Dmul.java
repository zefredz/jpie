package com.sootNsmoke.instructions;

public class Dmul extends NoArgsSequence
{
    public Dmul()
    {
        super(0, -2, opc_dmul);
    }
}
