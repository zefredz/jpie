package com.sootNsmoke.instructions;

public class Areturn  extends  NoArgsSequence
{
    public Areturn ()
    {
        super(0, -1, opc_areturn);
    }
}
