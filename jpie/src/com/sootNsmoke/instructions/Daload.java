package com.sootNsmoke.instructions;

public class Daload extends NoArgsSequence
{
    public Daload()
    {
        super(0, 0, opc_daload);
    }
}
