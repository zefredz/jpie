package com.sootNsmoke.instructions;

public class Lor extends NoArgsSequence
{
    public Lor()
    {
        super(0, -2, opc_lor);
    }
}
