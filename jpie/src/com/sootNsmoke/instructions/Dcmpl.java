package com.sootNsmoke.instructions;

public class Dcmpl extends NoArgsSequence
{
    public Dcmpl()
    {
        super(0, -3, opc_dcmpl);
    }
}
