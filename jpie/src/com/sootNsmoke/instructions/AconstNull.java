package com.sootNsmoke.instructions;

public class AconstNull extends NoArgsSequence
{
    public AconstNull()
    {
        super(1, 1, opc_aconst_null);
    }
}
