package com.sootNsmoke.instructions;

public class Sastore  extends  NoArgsSequence
{
    public Sastore ()
    {
        super(0, -3, opc_sastore);
    }
}
