package com.sootNsmoke.instructions;

public class Monitorenter extends NoArgsSequence
{
	public Monitorenter()
	{
		super(0, -1, opc_monitorenter);
	}
}
