package com.sootNsmoke.instructions;

public class Athrow  extends  NoArgsSequence
{
    public Athrow ()
    {
        super(0, -1, opc_athrow);
    }
}
