package com.sootNsmoke.instructions;

public class Freturn extends NoArgsSequence
{
    public Freturn()
    {
        super(0, -1, opc_freturn);
    }
}
