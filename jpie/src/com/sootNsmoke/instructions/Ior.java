package com.sootNsmoke.instructions;

public class Ior extends NoArgsSequence
{
    public Ior()
    {
        super(0, -1, opc_ior);
    }
}
