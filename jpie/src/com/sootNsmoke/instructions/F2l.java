package com.sootNsmoke.instructions;

public class F2l extends NoArgsSequence
{
    public F2l()
    {
        super(0, 1, opc_f2l);
    }
}
