package com.sootNsmoke.instructions;

public class Aaload extends NoArgsSequence
{
    public Aaload()
    {
        super(0, -1, opc_aaload);
    }
}
