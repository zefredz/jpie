package com.sootNsmoke.instructions;

public class Iadd extends NoArgsSequence
{
    public Iadd()
    {
        super(0, -1, opc_iadd);
    }
}
