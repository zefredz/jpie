package com.sootNsmoke.instructions;

public class I2s extends NoArgsSequence
{
    public I2s()
    {
        super(0, 0, opc_i2s);
    }
}
