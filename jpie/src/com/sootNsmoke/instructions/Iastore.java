package com.sootNsmoke.instructions;

public class Iastore  extends  NoArgsSequence
{
    public Iastore ()
    {
        super(0, -3, opc_iastore);
    }
}
