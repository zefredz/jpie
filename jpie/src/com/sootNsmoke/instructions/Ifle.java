package com.sootNsmoke.instructions;

public class Ifle  extends  LabelSequence
{
    public Ifle (String label)
    {
        super(0, -1, opc_ifle, label);
    }
}
