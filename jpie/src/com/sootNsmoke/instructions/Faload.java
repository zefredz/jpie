package com.sootNsmoke.instructions;

public class Faload extends NoArgsSequence
{
    public Faload()
    {
        super(0, -1, opc_faload);
    }
}
