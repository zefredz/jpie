package com.sootNsmoke.instructions;

public class Lushr extends NoArgsSequence
{
    public Lushr()
    {
        super(0, -1, opc_lushr);
    }
}
