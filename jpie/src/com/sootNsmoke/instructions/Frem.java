package com.sootNsmoke.instructions;

public class Frem extends NoArgsSequence
{
    public Frem()
    {
        super(0, -1, opc_frem);
    }
}
