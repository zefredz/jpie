package com.sootNsmoke.instructions;

public class D2i extends NoArgsSequence
{
    public D2i()
    {
        super(0, -1, opc_d2i);
    }
}
