package com.sootNsmoke.instructions;

public class I2c extends NoArgsSequence
{
    public I2c()
    {
        super(0, 0, opc_i2c);
    }
}
