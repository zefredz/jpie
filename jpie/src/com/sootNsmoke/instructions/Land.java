package com.sootNsmoke.instructions;

public class Land extends NoArgsSequence
{
    public Land()
    {
        super(0, -2, opc_land);
    }
}
