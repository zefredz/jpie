package com.sootNsmoke.instructions;

public class Pop2 extends NoArgsSequence
{
	public Pop2()
	{
		super(0, -2, opc_pop2);
	}
}
