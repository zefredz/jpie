package com.sootNsmoke.instructions;

public class Lshl extends NoArgsSequence
{
    public Lshl()
    {
        super(0, -1, opc_lshl);
    }
}
