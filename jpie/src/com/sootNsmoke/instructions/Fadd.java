package com.sootNsmoke.instructions;

public class Fadd extends NoArgsSequence
{
    public Fadd()
    {
        super(0, -1, opc_fadd);
    }
}
