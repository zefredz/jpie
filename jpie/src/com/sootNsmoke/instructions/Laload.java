package com.sootNsmoke.instructions;

public class Laload extends NoArgsSequence
{
    public Laload()
    {
        super(0, 0, opc_laload);
    }
}
