package com.sootNsmoke.instructions;

public class Pop extends NoArgsSequence
{
	public Pop()
	{
		super(0, -1, opc_pop);
	}
}
