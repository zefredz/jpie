package com.sootNsmoke.instructions;

public class Ineg extends NoArgsSequence
{
    public Ineg()
    {
        super(0, 0, opc_ineg);
    }
}
