package com.sootNsmoke.instructions;

public class Iaload  extends  NoArgsSequence
{
    public Iaload ()
    {
        super(0, -1, opc_iaload);
    }
}
