package com.sootNsmoke.instructions;

public class Ldiv extends NoArgsSequence
{
    public Ldiv()
    {
        super(0, -2, opc_ldiv);
    }
}
