package com.sootNsmoke.instructions;

public class Monitorexit extends NoArgsSequence
{
	public Monitorexit()
	{
		super(0, -1, opc_monitorexit);
	}
}
