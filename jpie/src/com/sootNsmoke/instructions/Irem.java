package com.sootNsmoke.instructions;

public class Irem extends NoArgsSequence
{
    public Irem()
    {
        super(0, -1, opc_irem);
    }
}
