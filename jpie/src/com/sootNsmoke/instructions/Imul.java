package com.sootNsmoke.instructions;

public class Imul extends NoArgsSequence
{
    public Imul()
    {
        super(0, -1, opc_imul);
    }
}
