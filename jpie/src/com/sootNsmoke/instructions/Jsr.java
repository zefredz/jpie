package com.sootNsmoke.instructions;

public class Jsr extends LabelSequence
{
    public Jsr(String label)
    {
        super(0, -1, opc_jsr, label);
    }
}

