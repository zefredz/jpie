package com.sootNsmoke.instructions;

public class Dup  extends  NoArgsSequence
{
    public Dup ()
    {
        super(1, 1, opc_dup);
    }
}
