package com.sootNsmoke.instructions;

public class Fmul extends NoArgsSequence
{
    public Fmul()
    {
        super(0, -1, opc_fmul);
    }
}
