package com.sootNsmoke.instructions;

public class Ddiv extends NoArgsSequence
{
    public Ddiv()
    {
        super(0, -2, opc_ddiv);
    }
}
