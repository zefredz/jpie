package com.sootNsmoke.instructions;

public class F2i extends NoArgsSequence
{
    public F2i()
    {
        super(0, 0, opc_f2i);
    }
}
