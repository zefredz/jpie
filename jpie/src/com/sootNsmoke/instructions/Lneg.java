package com.sootNsmoke.instructions;

public class Lneg extends NoArgsSequence
{
    public Lneg()
    {
        super(0, 0, opc_lneg);
    }
}
