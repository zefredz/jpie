package com.sootNsmoke.instructions;

public class L2f extends NoArgsSequence
{
    public L2f()
    {
        super(0, -1, opc_l2f);
    }
}
