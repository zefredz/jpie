package com.sootNsmoke.instructions;

public class Dsub extends NoArgsSequence
{
    public Dsub()
    {
        super(0, -2, opc_dsub);
    }
}
