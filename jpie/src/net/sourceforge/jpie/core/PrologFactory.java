/*
 * Created on 24-juin-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.core;

import java.io.File;
import java.io.InputStream;

import net.sourceforge.jpie.exception.InvalidFactException;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.exception.InvalidRuleException;
import net.sourceforge.jpie.exception.InvalidTheoryException;
import net.sourceforge.jpie.exception.UnknownProviderException;


/**
 * PrologFactory.java <br />
 * A factory for Prolog objects like theories, engine, clauses...
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologFactory
{
	private final PrologProvider provider; // null

	/**
	 * Creates a new PrologFactory using a fully qualified name of a PrologProvider.
	 * @param p the fully qualified name of a PrologProvider
	 */
	private PrologFactory(String s) throws Exception
	{
		PrologProvider p = (PrologProvider) Class.forName(s).newInstance();
		this.provider = p;
	}

	/**
	 * Returns a new Instance of a PrologFactory using the given PrologProvider fully qualified name.
	 * @param provider a PrologProvider fully qualified name.
	 * @return a new instance of PrologFactory.
	 * @throws UnknownProviderException if the provider does not exist.
	 */
	public static PrologFactory newInstance(String providerPath)
		throws UnknownProviderException
	{
		PrologFactory temp;
		try
		{
			temp = new PrologFactory(providerPath);
		}
		catch (Exception e)
		{
			throw new UnknownProviderException(
				e.getMessage() + ":" + providerPath);
		}
		return temp;
	}

	/**
	 * Returns a new PrologQuery from its String representation.
	 * @param query String representation of the PrologQuery.
	 * @return a new PrologQuery corresponding to the given string.
	 * @throws InvalidQueryException.
	 */
	public PrologQuery newQuery(String query) throws InvalidQueryException
	{
		// check if query strarts with "?-" if it is the case, remove it
		if (query.startsWith("?-"))
			query = query.substring(2);
		return provider.newQuery(query);
	}

	/**
	 * Returns a new PrologFact from its String representation.
	 * @param fact String representation of the PrologFact.
	 * @return a new PrologFact corresponding to the given string.
	 * @throws InvalidFactException
	 */
	public PrologFact newFact(String fact) throws InvalidFactException
	{
		return provider.newFact(fact);
	}
	
	/**
	 * Returns a new PrologRule from its String representation.
	 * @param fact String representation of the PrologRule.
	 * @return a new PrologRule corresponding to the given string.
	 * @throws InvalidFactException
	 */
	public PrologRule newRule(String rule) throws InvalidRuleException
	{
		return provider.newRule(rule);
	}

	/**
	 * Returns a new PrologTheory corresponding to the given source file name.
	 * @param theory - String containing the path to the theory source file.
	 * @return a new PrologTheory.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromFile(String source)
		throws InvalidTheoryException
	{
		return provider.newTheoryFromFile(source);
	}

	/**
	 * Creates a new PrologTheory from the given source file.
	 * @param source the source file for the Theory
	 * @return a new PrologTheory.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromFile(File source)
		throws InvalidTheoryException
	{
		return provider.newTheoryFromFile(source);
	}

	/**
	 * Creates a new PrologTheory from the given String
	 * @param theory the text of the theory.
	 * @return a new PrologTheory.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromString(String theory)
		throws InvalidTheoryException
	{
		return provider.newTheoryFromString(theory);
	}

	/**
	 * Creates a new PrologTheory from the given InputStream
	 * @param is from which the theory must be read.
	 * @return a new PrologTheory.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromStream(InputStream is)
		throws InvalidTheoryException
	{
		return provider.newTheoryFromStream(is);
	}

	/**
	 * Returns a new PrologEngine.
	 * @return a new PrologEngine.
	 */
	public PrologEngine newPrologEngine()
	{
		return provider.newPrologEngine();
	}
}
