/*
 * Created on 08-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.core;

import net.sourceforge.jpie.exception.InvalidArgumentException;
import net.sourceforge.jpie.structs.PrologConstant;
import net.sourceforge.jpie.structs.PrologFloat;
import net.sourceforge.jpie.structs.PrologInteger;
import net.sourceforge.jpie.structs.PrologList;
import net.sourceforge.jpie.structs.PrologStructure;
import net.sourceforge.jpie.structs.PrologTerm;

/**
 * PrologTermFactory.java <br />
 * Factory for a PrologTerm|#46; Must implemented for every Prolog interpreter.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public interface PrologTermFactory
{
	/**
	 * Returns a new PrologTerm built from the "native" Prolog term passed as argument&#46; 
	 * This method is a conversion method from the real Prolog interpreter terms to the
	 * representation of those terms in Java.
	 * @param o "native" Prolog term to built the term from.
	 * @return the term built from the "native" Prolog term passed as argument.
	 */
	public  PrologTerm convertTerm(Object o);
	
	public  PrologConstant newConstant(java.lang.Object o) throws InvalidArgumentException;
	
	public  PrologInteger newPrologInteger(int i);
	
	public  PrologFloat newPrologFloat(float f);
	
	public  PrologStructure newStructure(String name, PrologTerm[] args);
	
	public  PrologList newList();
}
