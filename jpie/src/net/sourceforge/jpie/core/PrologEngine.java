/*
 * Created on 01-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.core;

import net.sourceforge.jpie.exception.InvalidClauseException;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.exception.InvalidTheoryException;
import net.sourceforge.jpie.exception.QueryNotInitializedException;

/**
 * PrologEngine.java <br />
 * Represents a generic abstract Prolog machine&#46; This interface must be implemented for every Prolog interpreter
 * one want to interface with Java.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public interface PrologEngine
{

	// metaprogramming methods

	/**
	 * Consult (i&#46;e&#46; compile) the given theory.
	 * @param t the PrologTheory to consult.
	 * @throws InvalidTheoryException if the PrologTheory is not a valid one.
	 */
	public void consult(PrologTheory t) throws InvalidTheoryException;

	/**
	 * Adds the given Prolog fact or rule to the current theory&#46; No verification is made to ensure the consistency
	 * of the theory after adding the given clause.
	 * @param clause the fact or rule to add to the current theory.
	 * @throws InvalidClauseException if the given clause is not a valid rule or fact.
	 */
	public void assertz(PrologClause clause) throws InvalidClauseException;

	/**
	 * Removes the given fact or rule from the current theory&#46; No verification is made to ensure the consistency
	 * of the theory after removing the given clause.
	 * @param clause the fact or rule to remove from the current theory. 
	 * @throws InvalidClauseException if the given clause is not a valid rule or fact.
	 */
	public void retract(PrologClause clause) throws InvalidClauseException;
	
	// query resolution methods

	/**
	 * Returns the first PrologResult of the evaluation of the given PrologQuery&#46; The given PrologQuery is set as the current PrologQuery&#46;
	 * If there is no result, the method return an empty PrologResult object.
	 * @param q the PrologQuery to evaluate.
	 * @return the first PrologResult of the evaluation of the given PrologQuery.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 */
	//	PRE a PrologQuery is given.
	//	POST the given PrologQuery is set as the current PrologQuery.
	public PrologResult find(PrologQuery q) throws InvalidQueryException;

	/**
	 * Returns the next PrologResult of the evaluation of the current PrologQuery&#46; The current PrologQuery has to be set by the execution of
	 * a previous find() method&#46; If there is no result, the method return an empty PrologResult object.
	 * @return the next PrologResult of the evaluation of the given PrologQuery.
	 * @throws QueryNotInitializedException if the PrologQuery is not initialized (i&#46;e&#46; if one try to use findNext() before find().
	 */
	//	PRE the current PrologQuery is set.
	//	POST returns the next PrologResult for the evaluation of the currentQuery.
	public PrologResult findNext() throws QueryNotInitializedException;

	/**
	 * Returns a PrologResultSet containing all the Results for the given PrologQuery avaluation&#46;
	 * If there is no result, the method return an empty PrologResultSet object.
	 * @param q the PrologQuery to evaluate
	 * @return a PrologResultSet containing all the PrologResult for the PrologQuery evaluation.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 */
	public PrologResultSet findAll(PrologQuery q) throws InvalidQueryException;
	
	/**
	 * Same effect as find and findNext but ror multiple query resolution.
	 * @param q the query to evaluate
	 * @return the next PrologResult of the evaluation of the given PrologQuery.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 */
	public PrologResult findNext(PrologQuery q) throws InvalidQueryException;

}
