/*
 * Created on 24-juin-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.core;

import java.io.File;
import java.io.InputStream;

import net.sourceforge.jpie.exception.InvalidFactException;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.exception.InvalidRuleException;
import net.sourceforge.jpie.exception.InvalidTheoryException;


/**
 * PrologProvider.java <br />
 * AbstractFactory for Prolog objects&#46; Provides methods to create Prolog classes such as PrologTheory, PrologQuery 
 * and PrologEngine&#46; The Prolog implementation dependent part of the engine is enclosed in the implementing class and 
 * the related implementations for PrologQuery, PrologTheory.
 * and PrologEngine.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public interface PrologProvider
{
	/**
	 * Creates a new PrologFact corresponding to the String representation of the PrologFact&#46; 
	 * For example "path(cplusplus, java)".
	 * @param fact the text of the PrologFact.
	 * @return a new PrologFact from a String.
	 * @throws InvalidFactException
	 */
	public PrologFact newFact(String query) throws InvalidFactException;
	
	/**
	 * Creates a new PrologRule corresponding to the String representation of the PrologRule&#46; 
	 * For example "path(X, Y):-link(X,Z),path(Z,Y)".
	 * @param rule the text of the PrologRule.
	 * @return a new PrologRule from a String.
	 * @throws InvalidRuleException
	 */
	public PrologRule newRule(String query) throws InvalidRuleException;
	
	/**
	 * Creates a new PrologQuery corresponding to the String representation of the PrologQuery&#46; 
	 * For example "path(X, java)".
	 * @param query the text of the PrologQuery.
	 * @return a new PrologQuery from a String.
	 * @throws InvalidQueryException
	 */
	public PrologQuery newQuery(String query) throws InvalidQueryException;

	/**
	 * Creates a new PrologTheory from the given source file.
	 * @param source the name of the source file for the PrologTheory.
	 * @return a new PrologTheory corresponding to the given source file.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromFile(String source) throws InvalidTheoryException;
	
	/**
	 * Creates a new PrologTheory from the given source file.
	 * @param source the source file of the Theory.
	 * @return a new PrologTheory corresponding to the given source file.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromFile(File source) throws InvalidTheoryException;
	
	/**
	 * Creates a new PrologTheory from the given String.
	 * @param theory String containing the text of the Theory.
	 * @return a new PrologTheory corresponding to the given String.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromString(String theory) throws InvalidTheoryException;
	
	/**
	 * @param is source InputStream for the Theory.
	 * @return a new PrologTheory corresponding o the given source.
	 * @throws InvalidTheoryException
	 */
	public PrologTheory newTheoryFromStream(InputStream is) throws InvalidTheoryException;

	/**
	 * Returns a new PrologEngine.
	 * @return a new PrologEngine.
	 */
	public PrologEngine newPrologEngine();
}
