/*
 * Created on 01-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.core;

import net.sourceforge.jpie.exception.NoMoreResultException;

/**
 * PrologResultSet.java <br />
 * Represents a set of results of a query resolution.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public interface PrologResultSet
{
	/**
	 * Returns the size (i&#46;e&#46; the number of Results) of the PrologResultSet .
	 * @return the size of the PrologResultSet.
	 */
	public int size();

	/**
	 * Returns true if the PrologResultSet is empty (i&#46;e&#46; it contains no PrologResult).
	 * @return true if the PrologResultSet is empty.
	 */
	public boolean isEmpty();

	/**
	 * Indicates wether or not the PrologResultSet contains another PrologResult.
	 * @return true if the PrologResultSet contains another PrologResult, false else.
	 */
	public boolean hasNext();

	/**
	 * Returns the next PrologResult of the PrologResultSet.
	 * @return the next PrologResult of the PrologResultSet.
	 * @exception NoMoreResultException if there is no more result in the result set.
	 */
	public PrologResult next() throws NoMoreResultException;

	/**
	 * Returns an array representation of the PrologResultSet (i&#46;e&#46; an array containing the Results stored in the PrologResultSet).
	 * @return an array representation of the PrologResultSet.
	 */
	public PrologResult[] toArray();

	/**
	 * Returns true if the request evaluation that produced the ResultSet had got one or more solution.
	 * @return true if the request evaluation that produced the ResultSet had got one or more solution.
	 */
	public boolean hasSolution();
}
