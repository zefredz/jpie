/*
 * Created on 10-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.structs;

import net.sourceforge.jpie.exception.NoMoreTermException;

/**
 * PrologList.java <br />
 * Represents a Prolog list in Java.
 * @author Minne Fr�d�ric
 * @version 0.2
 */
public abstract class PrologList extends PrologStructure
{
	/**
	 * Returns the size of the list.
	 * @return the size of the list.
	 */
	public abstract int size();
	
	/**
	 * Returns true if the list has one more element.
	 * @return true if the list has one more element.
	 */
	public abstract boolean hasNext();
	
	/**
	 * Returns the next term contained in the list .
	 * @return the next term contained in the list .
	 * @exception NoMoreTermException thrown if the list is empty.
	 */
	// PRE the list is not empty.
	// POST the next element of the list is returned or an exception if the list is empty.
	public abstract PrologTerm next() throws NoMoreTermException;
	
	/**
	 * Adds the given term at the head of of the list. 
	 * @param arg the term to add at the head of the list.
	 */
	// PRE
	// POST the head of the list is set to arg while the tail of the list is set to the original list itself.
	public abstract void add(PrologTerm arg);
	
	/**
	 * Returns and removes the head of the list.
	 * @return the head of the list.
	 * @throws NoMoreTermException thrown if the list is empty.
	 */
	// PRE the list is not empty.
	// POST the head of the list is set to tail.getHead() while the tail of the list is set to tail.getTail().
	public abstract PrologTerm remove() throws NoMoreTermException;
	
	/**
	 * Returns true if the list is empty.
	 * @return true if the list is empty.
	 */
	public boolean isEmpty()
	{
		return size() == 0;
	}
	
	/**
	 * Returns the String representation of the List.
	 * @return the String representation of the List.
	 */
	public abstract String getStringRepresentation();
	
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return getStringRepresentation();
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologStructure#getArguments()
	 */
	public abstract PrologTerm[] getArguments();

	/**
	 * @see net.sourceforge.jpie.structs.PrologStructure#getArity()
	 */
	public int getArity()
	{
		return 2;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologStructure#getName()
	 */
	public String getName()
	{
		return ".";
	}
	
	/**
	 * Returns the head of the PrologList.
	 * @return the head of the PrologList.
	 */
	public abstract PrologTerm getHead();
	
	/**
	 * Returns the tail of the PrologList.
	 * @return the tail of the PrologList.
	 */
	public abstract PrologList getTail();
	
	/**
	 * Returns the array of the terms in the PrologList.
	 * @return the array of the terms in the PrologList.
	 */
	public abstract PrologTerm[] toArray();

}
