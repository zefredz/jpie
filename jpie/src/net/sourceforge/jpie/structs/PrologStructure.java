/*
 * Created on 07-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.structs;

/**
 * PrologStructure.java <br />
 * Represents a Prolog structure (i&#46;e&#46; a Prolog compound) in Java.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public abstract class PrologStructure implements PrologTerm
{
	/**
	 * Returns the name of the structure.
	 * @return the name of the structure.
	 */
	public abstract String getName();
	
	/**
	 * Returns the arity (i&#46;e&#46; the number of arguments) of the structure.
	 * @return the arity of the structure.
	 */
	public abstract int getArity();
	
	/**
	 * Returns the functor (name and arity)  representing the structure. 
	 * @return the functor representing the structure.
	 */
	public String getFunctor()
	{
		return getName()+"/"+getArity();
	}
	
	/**
	 * Returns the arguments of the structure in an array.
	 * @return array containing the arguments of the structure.
	 */
	public abstract PrologTerm[] getArguments();
	
	/**
	 * @see net.sourceforge.jpie.structs.PrologTerm#getStringRepresentation()
	 */
	public abstract String getStringRepresentation();

}
