/*
 * Created on 03-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.structs;

/**
 * PrologVariable.java <br />
 * Represents a Prolog logic variable in Java.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public abstract class PrologVariable implements PrologTerm
{
	/**
	 * Returns the value stored in the variable.
	 * @return the value stored in the variable
	 */
	public abstract PrologTerm getValue();

	/**
	 * Returns the name of the variable.
	 * @return the name of the variable.
	 */
	public abstract String getName();

	/**
	 * Indicates wether or not the variable is bound.
	 * @return true if the variable is bound, false else
	 */
	public abstract boolean isBound();
	
	/**
	 * Returns the String representation of the variable.
	 * @return the String representation of the variable.
	 */
	public abstract String getStringRepresentation();

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return getStringRepresentation();
	}

	/**
	 * Two Variables are equal if they have the same value, the name doesn't matter
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o)
	{
		if (o instanceof PrologVariable)
		{
			PrologVariable v = (PrologVariable) o;
			// two PrologVariable's are equal if they have the same value, thus the name doesn't matter...
			return getValue().equals(v.getValue());
		}
		return false;
	}

	/**
	 *  According to [Bloch2001] if two objects are equal they must have the same hashcode...
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		// according to [Bloch2001] if two objects are equal they must have the same hashcode...
		return getValue().hashCode();
	}
}
