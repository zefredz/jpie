/*
 * Created on 14-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.exception;

/**
 * InvalidArgumentException.java <br />
 * Thrown when trying to create a PrologConstant from an unsupported type of argument.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class InvalidArgumentException extends Exception
{

	/**
	 * Default constructor for an InvalidArgumentException.
	 */
	public InvalidArgumentException()
	{
		super();
	}

	/**
	 * Constructor for an InvalidArgumentException.
	 * @param arg0 error message.
	 */
	public InvalidArgumentException(String arg0)
	{
		super(arg0);
	}
}
