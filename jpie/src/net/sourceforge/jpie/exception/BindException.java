/*
 * Created on 07-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.exception;

/**
 * BindException.java <br />
 * Exception thrown when trying to bind an already bound variable.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class BindException extends Exception
{

	/**
	 * Default constructore for a BindException.
	 */
	public BindException()
	{
		super();
	}

	/**
	 * Constructor for a BindException.
	 * @param arg0 error message.
	 */
	public BindException(String arg0)
	{
		super(arg0);
	}

}
