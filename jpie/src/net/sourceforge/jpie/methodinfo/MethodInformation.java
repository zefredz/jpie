/*
 * Created on 23-sept.-2003 - 10:50:15
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of Java Method Information API.
 *
 * Java Method Information API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Java Method Information API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Java Method Information API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.methodinfo;

import java.lang.reflect.Method;

/**
 * MethodInformation.java <br />
 * @author Minne Fr�d�ric
 * @version 0.1
 * TODO if several methods with the same name...
 */
public class MethodInformation
{
	public final static String ARRAY_TYPE = "[";
	public final static String BOOLEAN_TYPE = "Z";
	public final static String VOID_TYPE = "V";
	public final static String BYTE_TYPE = "B";
	public final static String SHORT_TYPE = "S";
	public final static String CHAR_TYPE = "C";
	public final static String INT_TYPE = "I";
	public final static String LONG_TYPE = "J";
	public final static String FLOAT_TYPE = "F";
	public final static String DOUBLE_TYPE = "D";

	private final Method method;

	public MethodInformation(Method method)
	{
		this.method = method;
	}

	public MethodInformation(Class c, String method)
		throws NoSuchMethodException
	{
		Method[] methods = c.getDeclaredMethods();
		test : {
			for (int i = 0, n = methods.length; i < n; i++)
			{
				if (methods[i].getName().equals(method))
				{
					this.method = methods[i];
					break test;
				}
			}
			this.method = null;
			throw new NoSuchMethodException(
				"Method " + method + " not found in class " + c.getName());
		}
	}

	public String getSignature()
	{
		StringBuffer sig = new StringBuffer();
		sig.append(this.method.getName());
		sig.append("(");
		Class[] params = this.method.getParameterTypes();
		sig.append(")");
		Class ret = this.method.getReturnType();
		sig.append(ClassRepresentation.getClassRepresentation(ret));
		return sig.toString();
	}

	public String[] getParameters()
	{
		return new String[0];
	}

	public Method getMethod()
	{
		return this.method;
	}
}

class ClassRepresentation
{
	static String getClassRepresentation(Class c)
	{
		StringBuffer ret = new StringBuffer();
		if (c.getName().equals("void"))
			ret.append(MethodInformation.VOID_TYPE);
		else
			if (c.isPrimitive())
			{
				try
				{
					ret.append(getPrimitiveType(c));
				}
				catch (NotAPrimitiveTypeException e)
				{
					e.printStackTrace();
				}
			}
			else
				if (c.isArray())
					ret.append(c.getName());
				else
					ret.append(getNonPrimitiveType(c));
		return ret.toString();
	}

	private static String getNonPrimitiveType(Class c)
	{
		StringBuffer ret = new StringBuffer("L");
		ret.append("L");
		ret.append(c.getName());
		ret.append(";");
		return ret.toString();
	}

	private static String getPrimitiveType(Class c) throws NotAPrimitiveTypeException
	{
		String s = c.getName();
		if (s.equals("byte"))
			return MethodInformation.BYTE_TYPE;
		if (s.equals("short"))
			return MethodInformation.SHORT_TYPE;
		if (s.equals("char"))
			return MethodInformation.CHAR_TYPE;
		if (s.equals("boolean"))
			return MethodInformation.BOOLEAN_TYPE;
		if (s.equals("int"))
			return MethodInformation.INT_TYPE;
		if (s.equals("long"))
			return MethodInformation.LONG_TYPE;
		if (s.equals("float"))
			return MethodInformation.FLOAT_TYPE;
		if (s.equals("double"))
			return MethodInformation.DOUBLE_TYPE;
		else
			throw new NotAPrimitiveTypeException(c.getName());
	}
}
