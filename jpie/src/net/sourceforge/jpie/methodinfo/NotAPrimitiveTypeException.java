/*
 * Created on 25-sept.-2003 - 16:30:06
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of Java Method Information API.
 *
 * Java Method Information API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Java Method Information API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Java Method Information API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.methodinfo;

/**
 * NotAPrimitiveTypeException.java <br />
 * @author Minne Fr�d�ric
 * @version 1.0
 */
public class NotAPrimitiveTypeException extends Exception
{

	/**
	 * 
	 */
	public NotAPrimitiveTypeException()
	{
		super();
	}

	/**
	 * @param arg0
	 */
	public NotAPrimitiveTypeException(String arg0)
	{
		super(arg0);
	}

}
