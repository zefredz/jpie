/*
 * MessageWindow.java
 *
 * Created on 29 ao�t 2003, 23:08
 */
package net.sourceforge.jpie.ide;

/**
 *
 * @author  Minne Fr�d�ric
 */
public class MessageWindow extends javax.swing.JFrame {
    
    /** Creates new form MessageWindow */
    public MessageWindow() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        jScrollPane1 = new javax.swing.JScrollPane();
        msgArea = new javax.swing.JTextArea();
        hideButton = new javax.swing.JButton();

        setTitle("MessageWindow");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jScrollPane1.setDoubleBuffered(true);
        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setMinimumSize(new java.awt.Dimension(400, 200));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(400, 200));
        msgArea.setBackground(new java.awt.Color(204, 204, 204));
        msgArea.setEditable(false);
        jScrollPane1.setViewportView(msgArea);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        hideButton.setText("Hide");
        hideButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hideButtonActionPerformed(evt);
            }
        });

        getContentPane().add(hideButton, java.awt.BorderLayout.SOUTH);

        pack();
    }//GEN-END:initComponents

    private void hideButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hideButtonActionPerformed
        // Add your handling code here:
        hide();
    }//GEN-LAST:event_hideButtonActionPerformed
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        hide();
    }//GEN-LAST:event_exitForm
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new MessageWindow().show();
    } 
    
    public void addMessage(String message)
    {
        msgArea.append(message);
        msgArea.append("\n");
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton hideButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea msgArea;
    // End of variables declaration//GEN-END:variables
    
}
