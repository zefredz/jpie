/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import net.sourceforge.jpie.core.PrologTheory;
import net.sourceforge.jpie.exception.InvalidTheoryException;

import com.sootNsmoke.prolog.PrologCompiler;
import com.sootNsmoke.prolog.SyntaxError;


/**
 * PrologCompilerTheory.java <br />
 * Represents a Joshua Engel's Prolog compiler theory.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerTheory implements PrologTheory
{
	// stores the source of the theory
	private final Object src;
	
	// instance of Engel's Prolog compiler 
	private PrologCompiler compiler;
	
	/**
	 * Constructs a PrologCompilerTheory from a File.
	 * @param source the path to the source file for the PrologTheory.
	 * @throws InvalidTheoryException if the PrologTheory is not valid (IOException, FileNotFoundException...)
	 */
	PrologCompilerTheory(File source) throws InvalidTheoryException
	{
		src = source;
		try
		{
			compiler = new PrologCompiler(new FileInputStream((File) source));
		}
		catch (FileNotFoundException e)
		{
			throw new InvalidTheoryException("source not found : "  + source);
		}
	}
	
	/**
	 * Constructs a PrologCompilerTheory from an input stream.
	 * @param source the input stream that contains the theory.
	 * @throws InvalidTheoryException if the theory is not valid.
	 */
	PrologCompilerTheory(InputStream source) throws InvalidTheoryException
	{
		src = source;
		compiler = new PrologCompiler(source);
	}
	
	/**
	 * Constructs a PrologCompilerTheory from the text of the theory passed as a String. 
	 * @param theory text of the theory.
	 * @throws InvalidTheoryException if the theory is not valid
	 */
	PrologCompilerTheory(String theory) throws InvalidTheoryException
	{
		src = theory;
		compiler = new PrologCompiler(theory);
	}

	/**
	 * Compiles the PrologTheory.
	 * @throws InvalidTheoryException if the PrologTheory is not valid (SyntaxError).
	 */
	void consult() throws InvalidTheoryException
	{
		try
		{
			compiler.compile();
		}
		catch (SyntaxError e)
		{
			throw new InvalidTheoryException("syntax error : " + src.toString());
		}
		// to get rid of the PrologCompiler toString method bug
		catch (Throwable t)
		{
			// do nothing
		}
	}
}
