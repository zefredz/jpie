/*
 * Created on 03-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import net.sourceforge.jpie.core.PrologTermFactory;
import net.sourceforge.jpie.structs.PrologTerm;
import net.sourceforge.jpie.structs.PrologVariable;

/**
 * PrologCompilerVariable.java <br />
 * Represents a Joshua Engel's Prolog compiler variable.
 * @author Minne Fr�d�ric
 * @version 0.2
 */
public class PrologCompilerVariable extends PrologVariable
{
	// name of the variable
	private final String name;
	
	// binding of the variable
	private final PrologTerm value;
	
	// term factory
	private PrologTermFactory factory;
	
	/**
	 * Constructor.
	 * @param name the name of the PrologVariable.
	 * @param value the value to store in the variable.
	 */
	PrologCompilerVariable(String name, Object value)
	{
		factory = new PrologCompilerTermFactory();
		this.name = name;
		this.value = factory.convertTerm(value);
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologVariable#getName()
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologVariable#getValue()
	 */
	public PrologTerm getValue()
	{
		return value;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologVariable#isBound()
	 */
	public boolean isBound()
	{
		return value != null;
	}
	
	/**
	 * @see net.sourceforge.jpie.structs.PrologTerm#getStringRepresentation()
	 */
	public String getStringRepresentation()
	{
		return getName() + " = " + getValue().getStringRepresentation();
	}

}
