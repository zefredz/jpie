/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import java.io.File;
import java.io.InputStream;

import net.sourceforge.jpie.core.PrologEngine;
import net.sourceforge.jpie.core.PrologFact;
import net.sourceforge.jpie.core.PrologProvider;
import net.sourceforge.jpie.core.PrologQuery;
import net.sourceforge.jpie.core.PrologRule;
import net.sourceforge.jpie.core.PrologTheory;
import net.sourceforge.jpie.exception.InvalidFactException;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.exception.InvalidRuleException;
import net.sourceforge.jpie.exception.InvalidTheoryException;


/**
 * PrologCompilerProvider.java <br />
 * Concrete factory implemented for the Joshua Engel's Prolog compiler.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerProvider implements PrologProvider
{
	
	/**
	 * Default constructor for a PrologCompilerProvider.
	 */
	public PrologCompilerProvider()
	{
		// empty constructor
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newQuery(java.lang.String)
	 */
	public PrologQuery newQuery(String query) throws InvalidQueryException
	{
		if (query.startsWith("?-"))
			query = query.substring(2);
		if (! query.endsWith("."))
			query = query + ".";
		return new PrologCompilerQuery(query);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newTheoryFromFile(java.lang.String)
	 */
	public PrologTheory newTheoryFromFile(String source) throws InvalidTheoryException
	{
		return new PrologCompilerTheory(new File(source));
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newPrologEngine()
	 */
	public PrologEngine newPrologEngine()
	{
		return new PrologCompilerEngine();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newTheoryFromFile(java.io.File)
	 */
	public PrologTheory newTheoryFromFile(File source)
		throws InvalidTheoryException
	{
		return new PrologCompilerTheory(source);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newTheoryFromStream(java.io.InputStream)
	 */
	public PrologTheory newTheoryFromStream(InputStream is)
		throws InvalidTheoryException
	{
		return new PrologCompilerTheory(is);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newTheoryFromString(java.lang.String)
	 */
	public PrologTheory newTheoryFromString(String theory)
		throws InvalidTheoryException
	{
		return new PrologCompilerTheory(theory);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newFact(java.lang.String)
	 */
	public PrologFact newFact(String fact) throws InvalidFactException
	{
		return new PrologCompilerFact(fact);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologProvider#newRule(java.lang.String)
	 */
	public PrologRule newRule(String rule) throws InvalidRuleException
	{
		return new PrologCompilerRule(rule);
	}

}
