/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;


import java.util.Vector;
import java.util.List;

import net.sourceforge.jpie.core.PrologResult;
import net.sourceforge.jpie.core.PrologResultSet;
import net.sourceforge.jpie.exception.NoMoreResultException;

/**
 * PrologCompilerResultSet.java <br />
 * Represents a set of results for the Joshua Engel's Prolog compiler.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerResultSet implements PrologResultSet
{
	// list of results
	private List results;
	
	// index of current result in the list
	private int currentIndex;
	
	// true if the result set is a solution
	boolean solution = false;
	
	/**
	 * Constructor.
	 */
	PrologCompilerResultSet()
	{
		// use Vector as a List
		results = new Vector();
		currentIndex = 0;
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#size()
	 */
	public int size()
	{
		return results.size();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#hasNext()
	 */
	public boolean hasNext()
	{
		return currentIndex != results.size();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#next()
	 */
	public PrologResult next() throws NoMoreResultException
	{
		if (currentIndex == results.size())
			throw new NoMoreResultException("No more result");
		PrologResult temp = (PrologResult) results.get(currentIndex);
		currentIndex ++;
		return temp;
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#toArray()
	 */
	public PrologResult[] toArray()
	{
		Object[] o = results.toArray();
		PrologResult[] temp = new PrologResult[o.length];
		for (int i = 0, n = o.length; i < n; i++)
		{
			temp[i] = (PrologResult) o[i];
		}
		return temp;
	}
	
	/**
	 * Adds a PrologResult to the PrologResultSet.
	 * @param r the PrologResult to add.
	 */
	void addResult(PrologResult r)
	{
		results.add(r);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#isEmpty()
	 */
	public boolean isEmpty()
	{
		return size() == 0;
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResultSet#hasSolution()
	 */
	public boolean hasSolution()
	{
		return solution;
	}

}
