/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;


import java.util.Vector;
import java.util.List;

import net.sourceforge.jpie.core.PrologResult;
import net.sourceforge.jpie.structs.PrologVariable;

/**
 * PrologCompilerResult.java <br />
 * Store the result of the resolution of a query  by the Joshua Engel's Prolog compiler.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerResult implements PrologResult
{
	// list to store the substitution
	private List vars;
	
	// true if the result is a solution
	boolean solution = false;
	
	/**
	 * Constructor
	 */
	// constructor is only accessible from the package
	PrologCompilerResult()
	{
		vars = new Vector();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologResult#getSubstitution()
	 */
	public PrologVariable[] getSubstitution()
	{
		Object[] o = vars.toArray();
		PrologVariable[] temp = new PrologVariable[o.length];
		for (int i = 0, n = o.length; i < n; i++)
		{
			temp[i] = (PrologVariable) o[i];
		}
		return temp;
	}
	
	/**
	 * @see net.sourceforge.jpie.core.PrologResult#isEmpty()
	 */
	public boolean isEmpty()
	{
		return vars.size() == 0;
	}

	
	/**
	 * Adds a PrologVariable to the PrologResult.
	 * @param v the PrologVariable to add.
	 */
	// method only accessible from the package
	void addVar(PrologVariable v)
	{
		vars.add(v);
	}
	
	/**
	 * @see net.sourceforge.jpie.core.PrologResult#hasSolution()
	 */
	public boolean hasSolution()
	{
		return solution;
	}

}
