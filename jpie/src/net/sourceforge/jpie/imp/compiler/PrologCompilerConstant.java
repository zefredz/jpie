/*
 * Created on 07-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import net.sourceforge.jpie.structs.PrologAtom;
import net.sourceforge.jpie.structs.PrologConstant;
import net.sourceforge.jpie.structs.PrologFloat;
import net.sourceforge.jpie.structs.PrologInteger;
import net.sourceforge.jpie.structs.PrologNumber;

/**
 * PrologCompilerConstant.java <br />
 * Represents a Joshua Engel's Prolog compiler constant&#46; This single class represents every type of
 * Prolog constants.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerConstant
	implements PrologConstant, PrologAtom, PrologFloat, PrologInteger, PrologNumber
{
	// value of the constant
	private final String constant;
	
	/**
	 * Every Engel's Prolog Compiler constant is represented by a String.
	 * @param o Value of the constant.
	 */
	PrologCompilerConstant(Object o)
	{
		this.constant = (String) o;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologConstant#getValue()
	 */
	public Object getValue()
	{
		Object toret = null;
		try
		{
			// constant  is the String representation of an integer
			toret = Float.valueOf(this.constant);
		}
		catch(NumberFormatException e1)
		{
			try
			{
				// constant is the String representation of a float
				toret = Integer.valueOf(this.constant);
			}
			catch(NumberFormatException e2)
			{
				// constant is a String
				toret = this.constant;
			}
		}
		return toret;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologAtom#getString()
	 */
	public String getString()
	{
		return this.constant;
	}

	/**
	 * Returns the Float value of the constant or throws NumberFormatException if the Constant is not a Float.
	 * @see net.sourceforge.jpie.structs.PrologFloat#getFloat()
	 */
	public Float getFloat()
	{
		return Float.valueOf(this.constant);
	}

	/**
	 * Returns the Integer value of the constant or throws a NumberFormatException if the Constant is not an Integer.
	 * @see net.sourceforge.jpie.structs.PrologInteger#getInteger()
	 */
	public Integer getInteger()
	{
		return Integer.valueOf(this.constant);
	}

	/**
	 * Returns the Number value of the constant or throws a NumberFormatException if the Constant is not a Number.
	 * @see net.sourceforge.jpie.structs.PrologNumber#getNumber()
	 */
	public Number getNumber()
	{
		try
		{
			return Integer.valueOf(this.constant);
		}
		catch(NumberFormatException e)
		{
			// another NumberFormatException could be thrown here.
			return Float.valueOf(this.constant);
		}
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologTerm#getStringRepresentation()
	 */
	public String getStringRepresentation()
	{
		return this.constant;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologFloat#getPrimitiveFloat()
	 * @see net.sourceforge.jpie.imp.compiler.PrologCompilerConstant#getFloat()
	 */
	public float getPrimitiveFloat()
	{
		return getFloat().floatValue();
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologInteger#getPrimitiveInt()
	 * @see net.sourceforge.jpie.imp.compiler.PrologCompilerConstant#getInteger()
	 */
	public int getPrimitiveInt()
	{
		return getInteger().intValue();
	}

}
