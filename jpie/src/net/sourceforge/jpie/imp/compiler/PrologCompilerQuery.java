/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Vector;

import net.sourceforge.jpie.core.PrologQuery;
import net.sourceforge.jpie.core.PrologResult;
import net.sourceforge.jpie.core.PrologResultSet;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.structs.PrologVariable;


import com.sootNsmoke.prolog.Cons;
import com.sootNsmoke.prolog.Prolog;
import com.sootNsmoke.prolog.PrologParser;
import com.sootNsmoke.prolog.Structure;
import com.sootNsmoke.prolog.Var;

/**
 * PrologCompilerQuery.java <br />
 * Represents a Joshua Engel's Prolog compiler query.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerQuery implements PrologQuery
{
	// String to store the query
	private final String query;
	
	// instance of Engel's Prolog parser
	private PrologParser parser;
	
	// the following fields are for interpreter internal use
	private final Object command;
	private PrologResult[] temp;
	private Vector vars = null;
	private Structure s = null;
	private Class c = null;
	private Constructor[] inits = null;
	private Object pred = null;
	private Method[] m = null;
	private Prolog p = null;

	/**
	 * Constructor.
	 * @param s the String representation of the PrologQuery.
	 */
	public PrologCompilerQuery(String src) throws InvalidQueryException
	{
		this.query = src;
		parser = new PrologParser(src);
		try
		{
			command = parser.command();
			p = new Prolog();
			if (command instanceof Structure)
			{
				vars = new Vector();
				Prolog.findVars(command, vars);
				s = (Structure) command;

				c = Class.forName(s.functor + "_" + s.arg.length);
				inits = c.getConstructors();
				pred = inits[0].newInstance(new Object[] { p });

				m = c.getDeclaredMethods();
			}
		}
		catch (Exception e)
		{
			throw new InvalidQueryException(e.getMessage());
		}
	}

	/**
	 * Returns the next PrologResult of the evaluation of the PrologQuery.
	 * @return next PrologResult of the evaluation of the PrologQuery.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 * @throws NoResultException if no PrologResult to return.
	 */
	PrologResult find() throws InvalidQueryException
	{
		PrologCompilerResult r = new PrologCompilerResult();
		try
		{
			if (((Boolean) m[0].invoke(pred, s.arg)).booleanValue())
			{
				r.solution = true;
				for (int i = 0; i < vars.size(); i++)
				{
					Var v = (Var) vars.elementAt(i);
					PrologVariable vv = resolveVar(v);
					r.addVar(vv);
				}
			}
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			throw new InvalidQueryException(e.getMessage());
		}
		return r;
	}

	/**
	 * Returns a PrologResultSet containing all the Results for the PrologQuery evaluation.
	 * @return a PrologResultSet containing all the Results for the PrologQuery evaluation.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 * @throws NoResultException if there is no PrologResult for the evaluation.
	 */
	PrologResultSet findAll() throws InvalidQueryException
	{
		PrologResultSet results = interpret(command);
		return results;
	}

	/**
	 * Computes all the Results for a PrologQuery.
	 * @param command the Prolog compiler <i>native</i> PrologQuery representation. 
	 * @return all the Results in a PrologResultSet.
	 * @throws InvalidQueryException if the PrologQuery is not valid.
	 */
	private PrologResultSet interpret(Object command)
		throws InvalidQueryException
	{
		PrologCompilerResultSet results = new PrologCompilerResultSet();
		boolean firstTime = true;
		try
		{
			while (((Boolean) m[0].invoke(pred, s.arg)).booleanValue())
			{
				if(firstTime)
				{
					firstTime = false;
					results.solution = true;
				} 
				PrologCompilerResult r = new PrologCompilerResult();
				r.solution = true;
				for (int i = 0; i < vars.size(); i++)
				{
					Var v = (Var) vars.elementAt(i);
					PrologVariable vv = resolveVar(v);
					r.addVar(vv);
				}
				results.addResult(r);
			}
			return results;
		}
		catch (Exception e1)
		{
			throw new InvalidQueryException(e1.getMessage());
		}
	}

	/**
	 * Converts a <i>native</i> Prolog variable to a PrologCompilerVariable.
	 * @param v the variable to convert.
	 * @return the PrologCompilerVariable constructs from the <i>native</i> variable.
	 */
	private PrologVariable resolveVar(Var v)
	{
		Object binding = p.deref(v);
		PrologVariable vv;
		if (binding instanceof Cons)
			vv =
				new PrologCompilerVariable(
					v.toString(),
					new PrologCompilerList((Cons) binding));
		else
			vv = new PrologCompilerVariable(v.toString(), binding);
		return vv;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "PrologCompilerQuery:" + this.query;
	}

	/**
	 * Two PrologCompilerQueries are equal if they have the same String representation.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object arg0)
	{
		if (arg0 != null || this.getClass().equals(arg0.getClass()))
		{
			return this.query.equals(((PrologCompilerQuery) arg0).query);
		}
		return false;
	}

	/**
	 * If two PrologCompilerQueries are equal they must have the same hashcode&#46; Then hashCode function only uses
	 * the String respresentation of a PrologCompilerQuery.
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		return 13 * this.query.hashCode();
	}

}
