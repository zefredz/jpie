/*
 * Created on 08-ao�t-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import net.sourceforge.jpie.core.PrologTermFactory;
import net.sourceforge.jpie.exception.InvalidArgumentException;
import net.sourceforge.jpie.structs.PrologConstant;
import net.sourceforge.jpie.structs.PrologFloat;
import net.sourceforge.jpie.structs.PrologInteger;
import net.sourceforge.jpie.structs.PrologList;
import net.sourceforge.jpie.structs.PrologStructure;
import net.sourceforge.jpie.structs.PrologTerm;

import com.sootNsmoke.prolog.Cons;
import com.sootNsmoke.prolog.Prolog;
import com.sootNsmoke.prolog.Var;


/**
 * PrologCompilerTermFactory.java <br />
 * Factory for a Joshua Engel's Prolog compiler term.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerTermFactory implements PrologTermFactory
{
	// instance of Engel's Prolog runtime environement
	private Prolog prolog;
	
	/**
	 * Default constructor for a PrologCompilerTermFactory.
	 */
	PrologCompilerTermFactory()
	{
		prolog = new Prolog();
	}
	
	/**
	 * @see net.sourceforge.jpie.core.PrologTermFactory#convertTerm(java.lang.Object)
	 */
	public PrologTerm convertTerm(Object o)
	{
		// if o is already a PrologTerm return it (i.e. it has been converted before).
		if (o instanceof PrologTerm)
		{
			return (PrologTerm) o;
		}
		// if o is a Var convert it to a PrologVariable.
		if (o instanceof Var)
		{
			Var v = (Var) o;
			return new PrologCompilerVariable(v.toString(), prolog.deref(v.binding()));
		}
		// if o is a Cons convert it to a PrologList.
		if (o instanceof Cons)
		{
			return new PrologCompilerList((Cons) o);
		}
		// else o is a constant.
		return new PrologCompilerConstant(o);
	}
	
	
	/**
	 * @see net.sourceforge.jpie.core.PrologTermFactory#newConstant(java.lang.Object)
	 */
	public PrologConstant newConstant(Object o) throws InvalidArgumentException
	{
		if(o instanceof String || o instanceof Integer || o instanceof Float)
			return new PrologCompilerConstant(o.toString());
		else
			throw new InvalidArgumentException("Integer, Float or String intended but " + o.getClass().getName() + " received");
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologTermFactory#newList()
	 */
	public PrologList newList()
	{
		return new PrologCompilerList();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologTermFactory#newPrologFloat(float)
	 */
	public PrologFloat newPrologFloat(float f)
	{
		return new PrologCompilerConstant("" + f);
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologTermFactory#newPrologInteger(int)
	 */
	public PrologInteger newPrologInteger(int i)
	{
		return new PrologCompilerConstant("" + i);
	}

	/**
	 * Not supported !
	 * @see net.sourceforge.jpie.core.PrologTermFactory#newStructure(java.lang.String, net.sourceforge.jpie.structs.PrologTerm[])
	 */
	public PrologStructure newStructure(String name, PrologTerm[] args)
	{
		throw new UnsupportedOperationException("Operation not supported");
	}

}
