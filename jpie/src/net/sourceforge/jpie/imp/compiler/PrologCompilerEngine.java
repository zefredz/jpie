/*
 * Created on 02-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import net.sourceforge.jpie.core.PrologClause;
import net.sourceforge.jpie.core.PrologEngine;
import net.sourceforge.jpie.core.PrologQuery;
import net.sourceforge.jpie.core.PrologResult;
import net.sourceforge.jpie.core.PrologResultSet;
import net.sourceforge.jpie.core.PrologTheory;
import net.sourceforge.jpie.exception.InvalidClauseException;
import net.sourceforge.jpie.exception.InvalidQueryException;
import net.sourceforge.jpie.exception.InvalidTheoryException;
import net.sourceforge.jpie.exception.QueryNotInitializedException;

/**
 * PrologCompilerEngine.java <br />
 * Concrete implementation of the abstract Prolog engine with the Joshe Engel's Prolog compiler.
 * @author Minne Fr�d�ric
 * @version 0.1
 */
public class PrologCompilerEngine implements PrologEngine
{	
	private PrologCompilerQuery current = null;

	public PrologCompilerEngine()
	{
		// empty constructor
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologEngine#consult(net.sourceforge.jpie.core.PrologTheory)
	 */
	public void consult(PrologTheory t) throws InvalidTheoryException
	{
		if (t == null)
			throw new InvalidTheoryException("invalid query:null theory");
		try
		{
			((PrologCompilerTheory) t).consult();
		}
		catch (ClassCastException e)
		{
			throw new InvalidTheoryException(e.getMessage());
		}
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologEngine#find(net.sourceforge.jpie.core.PrologQuery)
	 */
	public PrologResult find(PrologQuery q) throws InvalidQueryException
	{
		if (q == null)
			throw new InvalidQueryException("invalid query:null query");
		try
		{
			current = (PrologCompilerQuery) q;
			// add current to queries
		}
		catch (ClassCastException e)
		{
			throw new InvalidQueryException(e.getMessage());
		}
		return current.find();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologEngine#findNext()
	 */
	public PrologResult findNext() throws QueryNotInitializedException
	{
		// find q in queries
		// if it exists return q.find()
		// else throw QueryNotInitializedException
		PrologResult temp = null;
		if (current != null)
		{
			try
			{
				return temp = current.find();
			}
			catch (InvalidQueryException e)
			{
				throw new RuntimeException(e.getMessage());
			}
		}
		else
			throw new QueryNotInitializedException();
	}
	
	/**
	 * @see net.sourceforge.jpie.core.PrologEngine#findNext(net.sourceforge.jpie.core.PrologQuery)
	 */
	public PrologResult findNext(PrologQuery q) throws InvalidQueryException
	{
		PrologCompilerQuery query;
		if (q == null)
			throw new InvalidQueryException("invalid query:null query");
		try
		{
			query = (PrologCompilerQuery) q;
		}
		catch (ClassCastException e)
		{
			throw new InvalidQueryException(e.getMessage());
		}
		return query.find();
	}

	/**
	 * @see net.sourceforge.jpie.core.PrologEngine#findAll(net.sourceforge.jpie.core.PrologQuery)
	 */
	public PrologResultSet findAll(PrologQuery q) throws InvalidQueryException
	{
		if (q == null)
			throw new InvalidQueryException("invalid query:null query");
		PrologCompilerQuery pq;
		try
		{
			pq = (PrologCompilerQuery) q;
		}
		catch (ClassCastException e)
		{
			throw new InvalidQueryException(e.getMessage());
		}
		return pq.findAll();
	}
	
	/**
	 * Not yet supported !
	 * @see net.sourceforge.jpie.core.PrologEngine#assertz(net.sourceforge.jpie.core.PrologClause)
	 */
	public void assertz(PrologClause clause) throws InvalidClauseException
	{
		throw new UnsupportedOperationException("Not yet supported");
	}

	/**
	 * Not yet supported !
	 * @see net.sourceforge.jpie.core.PrologEngine#retract(net.sourceforge.jpie.core.PrologClause)
	 */
	public void retract(PrologClause clause) throws InvalidClauseException
	{
		throw new UnsupportedOperationException("Not yet supported");
	}

}
