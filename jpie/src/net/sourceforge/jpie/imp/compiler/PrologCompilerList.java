/*
 * Created on 10-juil.-2003
 * 
 * Copyright 2003 Minne Fr�d�ric
 * 
 * This file is part of java prolog interface.
 *
 * java prolog interface is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * java prolog interface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */
package net.sourceforge.jpie.imp.compiler;

import net.sourceforge.jpie.exception.NoMoreTermException;
import net.sourceforge.jpie.structs.PrologList;
import net.sourceforge.jpie.structs.PrologTerm;

import com.sootNsmoke.prolog.Cons;
import com.sootNsmoke.prolog.Prolog;


/**
 * PrologCompilerList.java <br />
 * Represents a Joshua Engel's Prolog compiler list.
 * @author Minne Fr�d�ric
 * @version 0.2
 */
public class PrologCompilerList extends PrologList
{
	// Engel's Prolog compiler list.
	private Cons list;
	
	// used by the hasNext() and next() methods.
	private Cons current;
	private int num;
	private PrologCompilerTermFactory termfactory =
		new PrologCompilerTermFactory();
		
	PrologCompilerList()
	{
		list = new Cons(null, null);
		current = list;
		num = 0;
	}

	/**
	 * Constructor for a PrologCompilerList.
	 * @param c Engels' compiler list.
	 */
	PrologCompilerList(Cons c)
	{
		list = c;
		current = list;
		num = 0;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#hasNext()
	 */
	public boolean hasNext()
	{
		if (list == null)
			return false;
		return list.tail != null;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#next()
	 */
	public PrologTerm next() throws NoMoreTermException
	{
		if (current != null)
		{
			Object temp = current.head;
			if (temp == null)
				throw new NoMoreTermException();
			current = (Cons) current.tail;
			return termfactory.convertTerm(temp);
		}
		throw new NoMoreTermException();
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#size()
	 */
	public int size()
	{
		int i = 1;
		Cons l = list;
		if (l == null || l.head == null)
			return 0;
		while (l.tail != null)
		{
			i++;
			l = (Cons) l.tail;
		}
		return i;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#getStringRepresentation()
	 */
	public String getStringRepresentation()
	{
		return this.list.toString(new Prolog());
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologStructure#getArguments()
	 */
	public PrologTerm[] getArguments()
	{
		PrologTerm[] temp = { getHead(), getTail()};
		return temp;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#getHead()
	 */
	public PrologTerm getHead()
	{
		return termfactory.convertTerm(list.head);
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#getTail()
	 */
	public PrologList getTail()
	{
		return new PrologCompilerList((Cons) list.tail);
	}

	/**
	 * Not yet supported for the class PrologCompilerList &#46; Use hasNext() and next() methods instead.
	 * @see net.sourceforge.jpie.structs.PrologList#toArray()
	 */
	public PrologTerm[] toArray()
	{
		throw new UnsupportedOperationException(
			"Operation not yet supported in class " + getClass().getName());
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#add(net.sourceforge.jpie.structs.PrologTerm)
	 */
	public void add(PrologTerm arg)
	{
		Object temp = termfactory.convertTerm(arg);
		Cons newlist = new Cons(temp, list);
		this.list = newlist;
	}

	/**
	 * @see net.sourceforge.jpie.structs.PrologList#remove()
	 */
	public PrologTerm remove() throws NoMoreTermException
	{
		if (list.head == null)
			throw new NoMoreTermException();
		PrologTerm temp = termfactory.convertTerm(list.head);
		list = (Cons) list.tail;
		return temp;
	}

}
