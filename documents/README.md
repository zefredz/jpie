# Étude de l'interopérabilité de deux langages de programmation basée sur la machine virtuelle de Java

Mémoire présenté par Frédéric Minne - Promoteur : Baudouin Le Charlier
UCL - Année académique 2002-2003

## Liste des fichiers :

* version finale
	* summary.pdf : résumé en deux pages du mémoire
	* memoire.pdf : mémoire complet au format pdf
	* errata.pdf : erratum du mémoire
	* slides.pdf : diaporama de la présentation publique du mémoires
* documents intermédiaires :
	* article_main.pdf : document intermédiaire
	* jpi-expose-09072003.pdf : exposé intermédiaire du 9 juillet 2003
	* jpi-expose-17072003.pdf : exposé intermédiaire du 17 juillet 2003
* autres documents
	* memoire.ps.gz.ps : mémoire complet au format postscript compressé (gzip)

