# Java-Prolog Interoperabilirty engine

Archived from Sourceforge / June 2018
Last modification : February 2004

## Étude de l'interopérabilité de deux langages de programmation basée sur la machine virtuelle de Java

Mémoire présenté par Frédéric Minne - Promoteur : Baudouin Le Charlier
UCL - Année académique 2002-2003

Intégration d'un moteur d'inférence Prolog dans du code Javascript. 

- documents : contient les documents relatifs à mon mémoire
- jpir : contient le code source (et les classes compilées)

Ce projet utilise l'assembleur Java "Oolong" de Joshua Engel publié dans son livre "Programming for Java Virtual Machine".

[Ooolong/Gnoloo source code](https://github.com/ymasory/programming-for-the-jvm)